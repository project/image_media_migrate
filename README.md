# Image Media Migrate

This module give form with comfort configuration for batch
process which migrate image from image field in media field basically.
It can migrate image frome media or image field in media or image field.

For a full description of the module, visit the project page
[project page](https://www.drupal.org/project/image_media_migrate).

To submit bug reports and feature suggestions, or track changes
[issue queue](https://www.drupal.org/project/issues/image_media_migrate).


## Table of contents

- Features
- Requirements
- Installation
- Configuration
- Maintainers


## Features

- **Batch Processing**:
  - Efficiently process large numbers of nodes for smooth and scalable migrations.
- **Field-to-Field Migration**:
  - Migrate content from `image` fields to `media` fields and vice versa.
  - Supports both single-value and multi-value fields.
- **Comprehensive Validation**:
  - Ensures source and destination fields belong to the same content type.
  - Prevents invalid configurations, such as using the same field for both source and destination.
  - Checks required fields are set and guards against attempts to migrate multi-value fields into single-value fields.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration > Configuration > Media > Media Migrate.
2. Select the content type and fields for migration.
3. Run validation checks to ensure compatibility between fields.
4. Click on the "Run batch" button to start the migration process.

### Notes on Configuration:
- Both source and destination fields must belong to the same content type.
- Ensure all necessary fields are specified, and that you are not migrating multi-value fields into single-value fields.


## Maintainers

- [Borg_git](https://www.drupal.org/u/borg_git)

